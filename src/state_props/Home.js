import React, { Component } from 'react';
import { Text, View} from 'react-native';

class Demostate2 extends Component{
    state={myState:'Cập nhật state'}
    updateState=()=>this.setState({myState:'State được cập nhật'})
    render(){
        return(
           <View>
                <Text onPress={this.updateState}>
                {this.state.myState}
                </Text>
            </View>
        );
    }
}

export default Demostate2;