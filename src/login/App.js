import React, { Component } from 'react';
import { Text, View, Button, Alert, TextInput, StyleSheet } from 'react-native';

export default class HelloWorldApp extends Component {
  render() {
    return (
      <View style={ styles.container }>
        <Text style={{marginBottom:20 }}>LOGIN</Text>
        <TextInput
          style={styles.input}
        />
        <TextInput
        style={styles.input }
        />
        <Button
          title="Login"
          onPress={() => Alert.alert('Login success!!!!!!')}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1, justifyContent: "center", alignItems: "center",
  },
  input: {
    height: 40,width:350, borderColor: 'gray', borderWidth: 1,marginBottom:20,
  },
});

