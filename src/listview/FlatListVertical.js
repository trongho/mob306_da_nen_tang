import React from 'react';
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  Button,
  Image,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import DATA from './data.json';
// import Constants from 'expo-constants';


function Item({item}) {
  return (
    <View style={{flexDirection: 'row', justifyContent: 'space-between', flex: 1, alignItems: 'center'}}>
      {/* <Text style={styles.title}>{item.title}</Text>
      <Text style={styles.content}>{item.noidung}</Text>
      <TouchableOpacity style={{marginTop: 5}}>
        <Text
          style={styles.btnText}>
          Chi tiết
        </Text>
      </TouchableOpacity> */}
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{
            height: 50,
            width: 50,
            borderRadius: 25,
            overflow: 'hidden',
            marginRight: 8,
          }}
          resizeMode="contain"
          source={require('../listview/images/avatar.png')}
        />
        <View>
          <Text style={styles.title}>{item.title}</Text>
          <Text style={styles.content}>example@gmail.com</Text>
        </View>
      </View>
      <Text>Chi tiêt</Text>
    </View>
  );
}

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        horizontal
        data={DATA}
        renderItem={({item}) => <Item item={item} />}
        keyExtractor={item => item.id}
        ItemSeparatorComponent={() => (
          <View style={{height: 1, backgroundColor: '#000'}} />
        )}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // marginTop: Constants.statusBarHeight,
  },
  item: {
    backgroundColor: '#FFF',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    elevation: 3,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  content: {
    fontSize: 14,
  },
  btnText: {
    padding: 5,
    backgroundColor: 'blue',
    color: '#FFF',
    textAlign: 'center',
    borderRadius: 5,
  },
});
