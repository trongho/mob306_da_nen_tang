import React,{ Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'



class PostsList extends Component{
    render(){
        return(
            <View style={styles.container}>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate('ViewPost')}>
                    <Text style={styles.text}>Posts List Screen</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default PostsList;

const styles=StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#D3EDFF',
    },
    text:{
        fontSize:28,
        textAlign:'center',
        margin:10,
    }
});