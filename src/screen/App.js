import React from 'react'
import { createStackNavigator } from 'react-navigation-stack'
import PostsList from './PostsList'
import ViewPost from './ViewPost'
import AddPost from './AddPost'
import { createAppContainer } from 'react-navigation';

const AppNavigator=createStackNavigator(
    {
        PostsList:{
            screen:PostsList,
            navigationOptions:{
                title:'Danh sach',
            }
        },
        ViewPost:ViewPost,
        AddPost:AddPost,
    },
    {
        initialRouteName:'PostsList',
        defaultNavigationOptions:{
            headerTitleAlign:'center'
        }
    }
);

const AppContainer=createAppContainer(AppNavigator);
export default class App extends React.Component{
    render(){
        return <AppContainer/>;
    }
}
